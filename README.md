Portal FGA
================
Tema Portal FGA para o Noosfero.

[![pipeline status](https://gitlab.com/lappis-unb/projects/fga/portal/themes/badges/unstable_deploy/pipeline.svg)](https://gitlab.com/lappis-unb/projects/fga/portal/themes/commits/unstable_deploy)

Instruções de uso
=================

1. Clonar diretório
```
$ git clone https://gitlab.com/lappis-unb/projects/fga/portal/themes.git ~/.
```

2. Criar links simbólicos do tema ```unb-gama``` para dentro da pasta themes no core do noosfero
```
$ cd noosfero
$ cd public/designs/themes
$ ln -s ~/themes/unb-gama .
```

3. Para ter certeza que o link simbolico funcionou
```
$ ls -l
```

### [Troubleshoot](https://gitlab.com/lappis-unb/projects/fga/portal/themes/wikis/Troubleshoot)
