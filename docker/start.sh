#! /bin/bash
plugins="custom_forms html5_video people_block statistics video work_assignment"

/noosfero/script/noosfero-plugins enable $plugins

echo "e = Environment.default
e.theme = 'unb-gama'
e.save" | rails c

bundle exec rake assets:precompile
/noosfero/script/production run
sleep infinity
